#!/bin/bash
########################################################################
#
#
#
#
#
#
#
#
#
########################################################################


g++ -c differential.cpp -o differential.o -g -O2 -Wno-deprecated -Wno-write-strings  -Wno-unused-result -DALL
g++ -c -I `root-config --incdir` glmt.cpp -o glmt.o -g -O2 -Wno-deprecated -Wno-write-strings  -Wno-unused-result -lm

g++ differential.o glmt.o -I `root-config --incdir` `root-config --glibs` `root-config --libs` -o glmt

./glmt
