/***********************************************************************
 * 
 * 
 * 
 **Laser
 *   wl 			!Wavelength*micron
 *   w0		     	!Beam waist*micron
 **Particle
 *   diam		   	!Particle diameter*micron
 *   x0		     	!Particle PositionX*micron, Polarization
 *   y0		     	!Particle PositionY*micron
 *   z0		     	!Particle PositionZ*micron, Popagation of the incident beam
 *   mext			!Real refractive index surrounding medium    
 *   indr		   	!Real part of the refractive index of the particle
 *   indim		  	!Imaginary part of the refractive index of the particle    
 **Angle
 *   phi   			!Polarization
 *	 theta			!degree to Z-axis
 **output
 *	 dfteta			!differential in teta direction
 *	 dfphi			!differential in phi direction
 **********************************************************************/


#include <math.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
using namespace std;


void diff(double wl, double w0, double diam, 
		  double x0, double y0, double z0, 
		  double mext, double indr, double indim, 
		  double theta, double phi, double &dfteta, double &dfphi);
void anbn(double alpha, complex<double> refrel, int nstop);
void gnml(double w0, double wl, double x, double y, double z, int nmax);
void champs(double teta, double fi, complex<double> &eteta, complex<double> &efi, int nmax);

//globe
double micron = 1e-6;
double centi = 1e-2;
const int mxn = 3000;
const int mxm = 10;
double pi = acos(-1.00);
complex<double> zi(0.0,1.0);
complex<double>an[mxn];
complex<double>bn[mxn];
complex<double>gte[mxn][2*mxm+1];
complex<double>gtm[mxn][2*mxm+1];


#ifndef ALL
int main()
{
	double wl,w0,diam,x,y,z,mext,indr,indim,phi,theta,dfteta,dfphi;
	wl = 0.532;
	w0 = 5000.0;
	diam = 1.7;
	x=0;
	y=0;
	z=0;
	mext=1.0;
	indr=1.4;
	indim=0;
	
	phi=90;
	double tmin=0;
	double tmax=180;
	int tnum=180;
	double tgap=(tmax-tmin)/tnum;

	ofstream myfile;
	myfile.open ("check.txt");
	
		
	myfile << "theta     dfteta    dfphi" << endl;
	
	
	for(int i=0;i<=180;i++)
	{

		theta=tmin+i*tgap;
		diff(wl,w0,diam,x,y,z,mext,indr,indim,theta,phi,dfteta,dfphi);
		myfile << theta<<"   "<<dfteta<<"   "<<dfphi<<endl;
	}
	
	myfile.close();
	
	
	

//	cout<<setprecision(17)<<dfteta<<"    "<<dfphi<<endl;

	return 1;
}
#endif


void diff(double wl, double w0, double diam, 
		  double x0, double y0, double z0, 
		  double mext, double indr, double indim, 
		  double theta, double phi, double &dfteta, double &dfphi)
{
	complex<double> refrel(indr, -1.0*abs(indim));
	refrel = refrel/mext;
	double alpha = pi*diam/wl;
	int nmax = alpha+4.05*pow(alpha,1.0/3.0)+2;

	anbn(alpha,refrel,nmax);
	gnml(w0,wl,x0,y0,z0,nmax);

	double teta, phia;
	if(theta<180.0)
	{
		teta = theta;
		phia = phi;
	}else{
		teta = 360.0-theta;
		phia = phi + 180.0;
	}
	
	double deg2rad = pi/180.0;
	double tetaa = teta*deg2rad;
	double fi = phia*deg2rad;
	complex<double> eteta, efi;
	champs(tetaa,fi,eteta,efi,nmax);
	double iteta = (eteta*conj(eteta)).real();
	double iphi = (efi*conj(efi)).real();
	double i2diff = pow(wl*micron/centi,2.0)/4.0/pi/pi;
	dfteta=iteta*i2diff;
	dfphi=iphi*i2diff;	

	return;
}

void anbn(double alpha, complex<double> refrel, int nstop)
{
	if(nstop > mxn)
	{
		cout<<"Particle is too big"<<endl;
		return;
	}
	//nearbyint(alpha/pi)*pi
	int tempnear = int(alpha/pi+0.5);
	if(abs(alpha)-tempnear*pi < 1e-5) alpha=alpha-2e-5;
//	int nmx = max(nstop,int(abs(refrel*alpha)))+15;
	#ifdef __CINT__
		int nmx = TMath::Max(nstop,int(abs(refrel*alpha)))+15;
	#else
		int nmx = max(nstop,int(abs(refrel*alpha)))+15;
	#endif
	
	double dn1a[mxn], ta;
	complex<double>dn1m[mxn];
	complex<double> tm, dn3, dn0;
	for(int i=nmx; i>=nstop+1; i--)
	{
		ta=i/alpha;
		tm=ta/refrel;
		dn1a[nstop]=ta-1.0/(dn1a[nstop]+ta);
		dn1m[nstop]=tm-1.0/(dn1m[nstop]+tm);
	}
	for(int i=nstop; i>=2; i--)
	{
		ta=i/alpha;
		tm=ta/refrel;
		dn1a[i-1]=ta-1.0/(dn1a[i]+ta);
		dn1m[i-1]=tm-1.0/(dn1m[i]+tm);		
	}
	dn3 = -1.0 * zi;
	dn0 = 0.5*(1.0-exp(2.0*zi*alpha));
	for(int i=1; i<=nstop; i++)
	{
		ta=i/alpha;
		dn3=1.0/(ta-dn3)-ta;
		dn0=dn0*(dn3+ta)/(dn1a[i]+ta);
		an[i]=dn0*(dn1m[i]-refrel*dn1a[i])/(dn1m[i]-refrel*dn3);
		bn[i]=dn0*(refrel*dn1m[i]-dn1a[i])/(refrel*dn1m[i]-dn3);
	}
	
	return;
}


void gnml(double w0, double wl, double x, double y, double z, int nmax)
{
	double eps = 1e-14;
	int jmax = 1000;
	double s = wl/(2.0*pi*w0);
	double xad = x/w0;
	double yad = y/w0;
	double zad = s*z/w0;
	complex<double> z1 = 1.0+2.0*zi*zad;
	complex<double> xp(xad,yad);
	complex<double> xm(xad,-1.0*yad);	
	complex<double> a0 = s/z1;	
	complex<double> fi0 = exp(zi*zad/(s*s)-(xad*xad+yad*yad)/z1)/(2.0*z1);
	
	for(int n=1; n<=nmax; n++)
	{
		double n2 = n + 0.5;
		complex<double> ann = n2*a0;
		complex<double> fin = exp(-1.0*n2*n2*s*a0);
		double rn0=2.0*n*(n+1.0)/(2.0*n+1.0);
		//cout<<rn0<<endl;
		complex<double> sj = ann;
		complex<double> ss = sj;
		for(int j=1; j<=jmax; j++)
		{
			double tempj = 1.0/(j*(j+1));
			sj = sj*ann*ann*xp*xm*tempj;
			ss = ss+sj;
			if (abs(sj/ss) < eps) break;		
		}
		complex<double> f1=2.0*xad*ss;
		complex<double> f2=2.0*zi*yad*ss;
		complex<double> prod=fi0*fin*rn0;
		gtm[n][mxm]=prod*zi*f1;
		gte[n][mxm]=prod*f2;
	//	int mmax=min(n,mxm);
		#ifdef __CINT__
			int mmax=TMath::Min(n,mxm);
		#else
			int mmax=min(n,mxm);
		#endif		
		
		complex<double> gnm=1.0;
		complex<double> xpm1=1.0;
		complex<double> xmm1=1.0;
		complex<double> pim=-zi;
		double rnm=1.0;		
		for(int m=1; m<=mmax; m++)
		{
			complex<double> r1=xp;
			complex<double> r2=xm/(m+1.0);
			complex<double> r3=xm;
			complex<double> r4=xp/(m+1.0);
			prod=gnm*ann*ann/double(m);
			complex<double> t1=prod*xmm1*xm;
			complex<double> t2=prod*xpm1*xp;
			complex<double> f1=gnm*xmm1+t1*(r1+r2);
			complex<double> f2=gnm*xmm1+t1*(r1-r2);
			complex<double> f3=gnm*xpm1+t2*(r3+r4);
			complex<double> f4=gnm*xpm1+t2*(r3-r4);	
			
			for(int j=m+1; j<=jmax; j++)
			{
				double tempjm=1.0/(j*(j-m));
				prod=ann*ann*xp*xm*tempjm;
				t1=t1*prod;
				t2=t2*prod;
				double j1=j+1.0;
				double jm=j-m+1.0;
				r1=xp/jm;
				r2=xm/j1;
				r3=xm/jm;
				r4=xp/j1;
				complex<double> f1p=t1*(r1+r2);
				complex<double> f2p=t1*(r1-r2);
				complex<double> f3p=t2*(r3+r4);
				complex<double> f4p=t2*(r3-r4);
				f1=f1+f1p;
				f2=f2+f2p;
				f3=f3+f3p;
				f4=f4+f4p;
				double test=abs(f1p/f1);
			//	test=max(test,abs(f2p/f2));
			//	test=max(test,abs(f3p/f3));
			//	test=max(test,abs(f4p/f4));
			#ifdef __CINT__
				test=TMath::Max(test,abs(f2p/f2));
				test=TMath::Max(test,abs(f3p/f3));
				test=TMath::Max(test,abs(f4p/f4));		
			#else
				test=max(test,abs(f2p/f2));
				test=max(test,abs(f3p/f3));
				test=max(test,abs(f4p/f4));
			#endif			
				if (test < eps) break;							
			}
			prod=fi0*fin*pim*rnm;
			gtm[n][m+mxm]=prod*zi*f1;
			gte[n][m+mxm]=prod*f2;
			gtm[n][mxm-m]=prod*zi*f3;
			gte[n][mxm-m]=-prod*f4;
			gnm=gnm*ann/double(m);
			pim=-pim*zi;
			rnm=rnm*2.0/(2.0*n+1.0);
			xpm1=xpm1*xp;
			xmm1=xmm1*xm;		
		}
	}

	return;
}


void champs(double teta, double fi, complex<double> &eteta, complex<double> &efi, int nmax)
{
	double tet=teta;
	if(tet<1e-5) tet=1e-5;
	double cost=cos(tet);
	double sint=sin(tet);
	
	double pinm0=1.0/sint;
	double pinm1=cost*pinm0;
	double taunm=-sint;
	double pinm2;
	
	complex<double> steta=1.5*an[1]*gtm[1][mxm]*taunm;
	complex<double> sfi=1.5*bn[1]*gte[1][mxm]*taunm;
	
	for(int n=2; n<=nmax; n++)
	{
		pinm2=((2.0*n-1.0)*cost*pinm1-(n-1.0)*pinm0)/double(n);
		taunm=n*(cost*pinm2-pinm1);
		double cn=(2.0*n+1.0)/(n*(n+1.0));
		steta=steta+cn*an[n]*gtm[n][mxm]*taunm;
		sfi=sfi+cn*zi*bn[n]*gte[n][mxm]*taunm;
		pinm0=pinm1;
		pinm1=pinm2;
	}
	
	pinm0=1.0/sint;
	for(int m=1; m<=mxm; m++)
	{	
		pinm0=-pinm0*sint*(2.0*m-1.0);
		taunm=m*cost*pinm0;	
		complex<double> expimf=exp(zi*double(m)*fi);
		
		int n=m;
		double cn=(2.0*n+1.0)/(n*(n+1.0));
		complex<double> t1=gtm[n][m+mxm]*expimf;
		complex<double> t2=gtm[n][mxm-m]/expimf;
		complex<double> t3=gte[n][m+mxm]*expimf;
		complex<double> t4=gte[n][mxm-m]/expimf;
		complex<double> fnm=an[n]*taunm*(t1+t2)+zi*double(m)*bn[n]*pinm0*(t3-t4);
		steta=steta+cn*fnm;
		complex<double> gnm=double(m)*an[n]*pinm0*(t1-t2)+zi*bn[n]*taunm*(t3+t4);
		sfi=sfi+cn*gnm;
		pinm1=(2.0*m+1.0)*cost*pinm0;
		taunm=(m+1.0)*cost*pinm1-(2.0*m+1.0)*pinm0;
		
		n=m+1;
		cn=(2.0*n+1.0)/(n*(n+1.0));
		t1=gtm[n][m+mxm]*expimf;
		t2=gtm[n][mxm-m]/expimf;
		t3=gte[n][m+mxm]*expimf;
		t4=gte[n][mxm-m]/expimf;
		fnm=an[n]*taunm*(t1+t2)+zi*double(m)*bn[n]*pinm1*(t3-t4);
		steta=steta+cn*fnm;
		gnm=double(m)*an[n]*pinm1*(t1-t2)+zi*bn[n]*taunm*(t3+t4);
		sfi=sfi+cn*gnm;
		pinm2=pinm1;
		pinm1=pinm0;		
		
		for(int n=m+2; n<=nmax; n++)
		{
			double pinm=((2.0*n-1.0)*cost*pinm2-(n+m-1.0)*pinm1)/double(n-m);
			taunm=n*cost*pinm-(n+m)*pinm2;
			cn=(2.0*n+1.0)/(n*(n+1.0));
			t1=gtm[n][m+mxm]*expimf;
			t2=gtm[n][mxm-m]/expimf;
			t3=gte[n][m+mxm]*expimf;
			t4=gte[n][mxm-m]/expimf;
			fnm=an[n]*taunm*(t1+t2)+zi*double(m)*bn[n]*pinm*(t3-t4);
			steta=steta+cn*fnm;
			gnm=double(m)*an[n]*pinm*(t1-t2)+zi*bn[n]*taunm*(t3+t4);
			sfi=sfi+cn*gnm;
			pinm1=pinm2;
			pinm2=pinm;			
		}		
	}	
	eteta=steta*zi;
	efi=-sfi;
	
	return;
}
