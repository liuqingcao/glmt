/***********************************************************************
 * 
 * 
 * run: ./run.sh (also with "root glmt.cpp", but much MUCH slower)
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 **********************************************************************/
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <TString.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TApplication.h>
#include <TVirtualFFT.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TStyle.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGaxis.h>
#include <TF1.h>
#include <math.h>
using namespace std; 

void ParticleType(int n);
void LaserPulse(double power, double centerwl, double fwhm, double beamwaist, double focuslength);
double PulseDist(double x);
double LaserDist(double x);
void SetPMT(double distance, double pmtdiam);
void ChangePosition(string axis, double min, double max, int num, double theta, double phi);
void ChangeAngle(string angle, double min, double max, int num, double another);
void Plot(string title[], string xlabel[], string ylabel[], int num, double xyz[], double dfteta[], double dfphi[], double dfnum[]);
void SetStyle(TMultiGraph *h1, string title, string Xtitle, string Ytitle);
void Process(string work , int i, int num);

void diff(double wl, double w0, double diam, 
		  double x0, double y0, double z0, 
		  double mext, double indr, double indim, 
		  double theta, double phi, double &dfteta, double &dfphi);
 
string particle;
const int wlnumber = 10;
double diam, mext, indr, indim, PMT, SpotArea;
string pmt_dist, pmt_diam;
double photon, wavelength, focuswaist;
double wltheta, wlmin, wlmax, wlambda;
double wlnow[wlnumber], wlratio[wlnumber], wlnum[wlnumber];
double pai = acos(-1.00);
double h = 6.62e-34;
double c = 3.0e8;


#ifndef __CINT__
	int main(int argc, char *argv[])
#else
	int glmt()
#endif
{
	#ifdef __CINT__
		//load the differential function
		gROOT->ProcessLine(".L differential.cpp");
	#else
		TApplication* rootapp = new TApplication("plot",&argc, argv);
	#endif

	//pulse energy/uJ; center wavelength/nm; FWHM/nm; beam waist/cm; focus length/cm; 
	LaserPulse(100.0, 761, 229, 1.0, 50.0);
	
	//0:UserDefine; 1:SiO2; 2:ZrO2; 3:TiO2; 4:Si; 5:Au;
	ParticleType(1);
	
	//distance to the center point/cm; diameter of the PMT window;
	SetPMT(5.0, 1.0);

	//"x/y/z"; min/um; max/um; number; Theta; Phi;
	ChangePosition("y",-15.0,15.0,300,30.0,90.0);
	
	//"theta/t/phi/p"; min; max; number; Phi/Theta
	ChangeAngle("theta",0.0,180.0,180,90.0);


	#ifndef __CINT__
		rootapp->Run();
	#endif	
	return 1;
}

void LaserPulse(double power, double centerwl, double fwhm, double beamwaist, double focuslength)
{
	wavelength = centerwl/1000.0;
	cout<<"Laser Center Wavelength: "<<wavelength*1000<<" nm"<<endl;	
	
	focuswaist = wavelength*focuslength/pai/beamwaist;
	SpotArea = pai*focuswaist*focuswaist*1e-8;
	cout<<"Beam Waist Radius: "<<focuswaist<<" um"<<endl;

	
	double wlgap = 2.0;
	wltheta = fwhm/1000.0/2.35482;
	wlmin = wavelength - wlgap * wltheta;
	#ifdef __CINT__	
		wlmin = TMath::Max(0.0, wlmin);
	#else
		wlmin = max(0.0, wlmin);
	#endif		
	wlmax = wavelength + wlgap * wltheta;

	cout<<"Pulse Energy: "<<power<<" uJ"<<endl;
	
	double hc = h*c*1e6;
	wlambda = (wlmax - wlmin)/wlnumber;
	double energy = 0;
	for(int i=0; i<wlnumber; i++)
	{
		wlnow[i] = wlmin + (i+0.5)*wlambda;
		wlratio[i] = PulseDist(wlnow[i]);
		energy += hc*wlratio[i]/wlnow[i];
	}
	double centernum = power/1e6/energy;
	photon = 0;
	for(int i=0; i<wlnumber; i++)
	{
		wlnum[i] = centernum*wlratio[i];
		photon += wlnum[i];
	}
	cout<<"Total Photon Number: "<<photon<<endl;
	
	return;
}

double PulseDist(double x)
{
	return exp(-0.5*pow(((x-wavelength)/wltheta),2));
}

double LaserDist(double x)
{
	return exp(-2.0*pow((x/focuswaist),2));
}

void ParticleType(int n)
{
	mext = 1.0;	//in the vacuum  diam, mext, indr, indim;
	switch (n)
	{
		case 0:
		{
			particle = "UserDefine";
			diam = 1.7;
			indr = 1.4;
			indim = 0.0;
			break;
		}
		case 1:
		{
			particle = "SiO_{2}";
			diam = 0.1;
			indr = 1.53874;
			indim = 0.0;			
			break;
		}
		case 2:
		{
			particle = "ZrO_{2}";
			diam = 0.06;
			indr = 2.18419;
			indim = 0.0;			
			break;
		}			
		case 3:
		{
			particle = "TiO_{2}";
			diam = 0.06;
			indr = 2.52492;
			indim = 0.0;			
			break;			
		}		
		case 4:
		{
			particle = "Si";
			diam = 0.06;
			indr = 3.7932;
			indim = 7.4796e-3;			
			break;
		}
		case 5:
		{
			particle = "Au";
			diam = 0.015;
			indr = 0.17538;
			indim = 4.9123;			
			break;
		}			
		default:
		{
			cout<<"Particle type is not exist, use Userdefine by default."<<endl;
			ParticleType(0);
		}
	}
	cout<<"Particle type: "<<particle<<endl;
	return;
}


void SetPMT(double distance, double pmtdiam)
{
	PMT = 2.0*pai*(1.0-sqrt(1.0/(1.0+pow(0.5*pmtdiam/distance,2))))/pow(distance,2);
	cout<<"PMT ratio: "<<PMT<<endl;
	ostringstream pmt_dist_s, pmt_diam_s;
	pmt_dist_s << distance; pmt_dist = pmt_dist_s.str();
	pmt_diam_s << pmtdiam;  pmt_diam = pmt_diam_s.str();
	return;
}

void ChangePosition(string axis, double min, double max, int num, double theta, double phi)
{
	const int ScanMax = num+1;
	double xyz[ScanMax], dfteta[ScanMax], dfphi[ScanMax], dfnum[ScanMax];
	double gap = (max-min)/num;
	if(axis=="x" || axis=="X")
	{
		for(int i=0;i<=num;i++)
		{
			Process("Scan Position: ", i, num);
			double x=min+i*gap;
			xyz[i]=x;
			diff(wavelength,focuswaist,diam,x,0.0,0.0,mext,indr,indim,theta,phi,dfteta[i],dfphi[i]);
			dfnum[i] = 0;
			for(int j=0; j<wlnumber; j++)
			{
				double df1, df2;
				diff(wlnow[j],focuswaist,diam,0.0,0.0,0.0,mext,indr,indim,theta,phi,df1,df2);
				dfnum[i] += wlnum[j]*(df1+df2);
			}
			dfnum[i] = dfnum[i]*PMT*LaserDist(x)/SpotArea;	
		}
	}else if(axis=="y" || axis=="Y"){
		for(int i=0;i<=num;i++)
		{	
			Process("Scan Position: ", i, num);	
			double y=min+i*gap;
			xyz[i]=y;
			diff(wavelength,focuswaist,diam,0.0,y,0.0,mext,indr,indim,theta,phi,dfteta[i],dfphi[i]);
			dfnum[i] = 0;
			for(int j=0; j<wlnumber; j++)
			{
				double df1, df2;
				diff(wlnow[j],focuswaist,diam,0.0,0.0,0.0,mext,indr,indim,theta,phi,df1,df2);
				dfnum[i] += wlnum[j]*(df1+df2);
			}
			dfnum[i] = dfnum[i]*PMT*LaserDist(y)/SpotArea;
		}		
	}else if(axis=="z" || axis=="Z"){
		for(int i=0;i<=num;i++)
		{	
			Process("Scan Position: ", i, num);	
			double z=min+i*gap;
			xyz[i]=z;
			diff(wavelength,focuswaist,diam,0.0,0.0,z,mext,indr,indim,theta,phi,dfteta[i],dfphi[i]);
			dfnum[i] = 0;
			for(int j=0; j<wlnumber; j++)
			{
				double df1, df2;
				diff(wlnow[j],focuswaist,diam,0.0,0.0,0.0,mext,indr,indim,theta,phi,df1,df2);
				dfnum[i] += wlnum[j]*(df1+df2);
			}
			dfnum[i] = dfnum[i]*PMT/SpotArea;
		}
	}else{
		cout<<"Change Postion: axis wrong!"<<endl;
		return;
	}
	
	ostringstream sstream1, sstream2;
	sstream1 << theta; sstream2 << phi;
	string view = "(" + sstream1.str() + "," + sstream2.str() + ")";
	
	string title[2], xlabel[2], ylabel[2];
	title[0] = particle + ": Differential Scattering Crossing Section " + view;
	xlabel[0] = axis + " Position (#mum)";
	ylabel[0] = "Diff. Crossing Section (cm^{2}/sr)";
	title[1] = particle + ": Photon number " + view + 
			" L=" + pmt_dist + "cm " + "D=" + pmt_diam + "cm";
	xlabel[1] = axis + " Position (#mum)";
	ylabel[1] = "Photon numbers";
	Plot(title, xlabel, ylabel, num+1, xyz, dfteta, dfphi, dfnum);

	return;
}

void ChangeAngle(string angle, double min, double max, int num, double another)
{
	string title[2], xlabel[2], ylabel[2];	
	string view;
	const int ScanMax = num+1;
	double degree[ScanMax], dfteta[ScanMax], dfphi[ScanMax], dfnum[ScanMax];
	double gap = (max-min)/num;
	if(angle=="theta" || angle=="t")
	{
		ostringstream sstream;
		sstream << another;
		view = "(#theta," + sstream.str() + ")";
		xlabel[0] = "#theta Angle (deg)";
		xlabel[1] = "#theta Angle (deg)";		
		for(int i=0;i<=num;i++)
		{
			Process("Scan Angle: ", i, num);
			double theta=min+i*gap;
			degree[i]=theta;
			diff(wavelength,focuswaist,diam,0.0,0.0,0.0,mext,indr,indim,theta,another,dfteta[i],dfphi[i]);
			dfnum[i] = 0;
			for(int j=0; j<wlnumber; j++)
			{
				double df1, df2;
				diff(wlnow[j],focuswaist,diam,0.0,0.0,0.0,mext,indr,indim,theta,another,df1,df2);
				dfnum[i] += wlnum[j]*(df1+df2);
			}
			dfnum[i] = dfnum[i]*PMT/SpotArea;
		}
	}else if(angle=="phi" || angle=="p"){
		ostringstream sstream;
		sstream << another;
		view = "(" + sstream.str() + ",#phi)";
		xlabel[0] = "#phi Angle (deg)";
		xlabel[1] = "#phi Angle (deg)";	
		for(int i=0;i<=num;i++)
		{	
			Process("Scan Angle: ", i, num);
			double phi=min+i*gap;
			degree[i]=phi;
			diff(wavelength,focuswaist,diam,0.0,0.0,0.0,mext,indr,indim,another,phi,dfteta[i],dfphi[i]);
			dfnum[i] = 0;
			for(int j=0; j<wlnumber; j++)
			{
				double df1, df2;
				diff(wlnow[j],focuswaist,diam,0.0,0.0,0.0,mext,indr,indim,another,phi,df1,df2);
				dfnum[i] += wlnum[j]*(df1+df2);
			}
			dfnum[i] = dfnum[i]*PMT/SpotArea;
		}		
	}else{
		cout<<"Change Angle: angle type wrong!"<<endl;
		return;
	}

	title[0] = particle + ": Differential Scattering Crossing Section " + view;
	ylabel[0] = "Diff. Crossing Section (cm^{2}/sr)";
	title[1] = particle + ": Photon number " + view + 
			" L=" + pmt_dist + "cm " + "D=" + pmt_diam + "cm";
	ylabel[1] = "Photon numbers";
	Plot(title, xlabel, ylabel, ScanMax, degree, dfteta, dfphi, dfnum);

	return;
}

void Plot(string title[], string xlabel[], string ylabel[], int num, double xyz[], double dfteta[], double dfphi[], double dfnum[])
{
	for(int i=0;i<=num;i++)
	{	
		if(dfteta[i]<1e-20) dfteta[i]=0.0;
		if(dfphi[i]<1e-20) dfphi[i]=0.0;
	}
	
	
	gROOT->SetBatch(false);
	TGaxis::SetMaxDigits(3);
	gStyle->SetTitleSize(0.1,"t");
	string cname = particle + ": " + xlabel[0];
	TCanvas *cc = new TCanvas(cname.c_str(),cname.c_str(),1200,600);
	cc->ToggleEventStatus();
	cc->ToggleEditor();
	cc->ToggleToolBar();
	cc->ToggleToolTips();
	cc->Divide(2,1);
	cc->cd(1);
	gPad->SetMargin(0.2,0.05,0.2,0.15);
	gPad->SetGrid();
	gPad->SetLogy();
	TMultiGraph *mg1 = new TMultiGraph();
	TLegend *legend = new TLegend(0.7,0.7,0.95,0.84);
	legend->SetFillColor(kWhite);
	legend->SetNColumns(1);
	legend->SetMargin(0.3);
	legend->SetBorderSize(0);	
	//Teta
	TGraph *g1 = new TGraph(num, xyz, dfteta);
	g1->SetLineColor(kBlue);
	g1->SetLineWidth(2);
	mg1->Add(g1);
	legend->AddEntry(g1,"#sigma_{Horizontal}","l");
	//Phi
	TGraph *g2 = new TGraph(num, xyz, dfphi);
	g2->SetLineColor(kRed);
	g2->SetLineWidth(2);
	mg1->Add(g2);
	legend->AddEntry(g2,"#sigma_{Vertical}","l");	
	//			
	mg1->Draw("AL");
	legend->Draw();
	SetStyle(mg1, title[0], xlabel[0], ylabel[0]);

		
	cc->cd(2);
	gPad->SetMargin(0.2,0.1,0.2,0.15);
	gPad->SetGrid();	
	TMultiGraph *mg2 = new TMultiGraph();	
	TGraph *g4 = new TGraph(num, xyz, dfnum);
	g4->SetLineColor(kBlue);
	g4->SetLineWidth(2);
	mg2->Add(g4);	
	mg2->Draw("AL");
	SetStyle(mg2, title[1], xlabel[1], ylabel[1]);	
	
	cc->Update();
	

	return;
}

void SetStyle(TMultiGraph *h1, string title, string Xtitle, string Ytitle)
{
	h1->SetTitle(title.c_str());	
	h1->GetXaxis()->SetTitle(Xtitle.c_str());
	h1->GetXaxis()->CenterTitle();	
	h1->GetXaxis()->SetTitleSize(0.05);
	h1->GetXaxis()->SetTitleOffset(1.3);
	h1->GetXaxis()->SetLabelSize(0.05);
	h1->GetXaxis()->SetLabelOffset(0.025);
	h1->GetXaxis()->SetNdivisions(9,5,0,kTRUE);
	
	h1->GetYaxis()->SetTitle(Ytitle.c_str());
	h1->GetYaxis()->CenterTitle();	
	h1->GetYaxis()->SetTitleSize(0.05);
	h1->GetYaxis()->SetTitleOffset(2.0);
	h1->GetYaxis()->SetLabelSize(0.05);
	h1->GetYaxis()->SetLabelOffset(0.025);
	h1->GetYaxis()->SetNdivisions(9,5,0,kTRUE);
}

void Process(string work , int i, int num)
{
	if(i==0) cout<<work<<flush;
	double precent = double(num)/10.0;
	double now = double(i)/precent;
	if( now == int(now)) cout<<i*10.0/precent<<"%--"<<flush;
	if(i==num) cout<<"Done"<<endl;

	return;
}
